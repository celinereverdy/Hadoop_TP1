# TP1 Hadoop, Céline REVERDY (ING5 BDA1)

# Objectif : effectuer une rotation de fichier csv en utilisant la méthode MapReduce

# On considère comme exemple le fichier csv suivant :
# "Hadoop",	"Spark"
# 1,			2
# 3,			4
# 5,			6

############ INPUT READER ############
# La première étape, "input reader", va créer une liste de la forme list(k1,v1) à partir du fichier d'origine
# où "k1" correspond au numéro de la ligne
# et "v1" correspond aux différentes valeurs présentes sur cette ligne, dans leur ordre d'origine (par colonne croissante)
# Ainsi, la liste obtenue est :
# [(1, ("Hadoop","Spark")),
#  (2, (1,2)),
#  (3, (3,4)),
#  (4, (5,6))]

############ MAP ############
# Pour chaque élément (k1,v1) de la liste, on crée une map avec la fonction suivante :

def Map(rowNumber, rowValues): # par exemple : Map(1, ("Hadoop","Spark"))
    colNumber = 1 # On initialise un compteur représentant le numéro de la colonne
    list_map = [] # On initialise une liste qui sera retournée à la fin, de la forme list(k2,v2)
    # Pour chaque élément de rowValues (chaque valeur de la ligne)
    for value in rowValues:
        # On ajoute à list_map un tuple de la forme (k2,v2) avec "k2"=numéro de la colonne et "v2"=(numéro de la ligne, valeur)
        list_map.append((colNumber,(rowNumber,value)))
        colNumber += 1 # On incrémente le numéro de la colonne
    # On retourne la liste list(k2,v2)
    # Par exemple, Map(1, ("Hadoop","Spark")) renvoie [(1, (1,"Hadoop")), (2, (1, "Spark")]
    return list_map

############ SHUFFLE/SORT ############
# L'étape "shuffle and sort" va récupérer toutes les maps créées précédemment et faire une liste
# en regroupant les éléments ayant le même numéro de colonne puis en triant ces éléments par numéro de ligne
# On obtient ainsi la liste suivante : 
# [(1, [(1, "Hadoop"), (2, 1), (3, 3), (4, 5)]), 
#  (2, [(1, "Spark"), (2, 2), (3, 4), (4, 6)])]

############ REDUCE ############
# Pour chaque élément (correspondant à une colonne) de cette liste, on applique la fonction suivante :

def Reduce(colNumber, list_colValues): # par exemple : Reduce(1, [(1, "Hadoop"), (2, 1), (3, 3), (4, 5)])
    list_values = [] # On initialise une liste qui va regrouper toutes les valeurs de la colonne
    # Pour chaque élément de list_colValues, par exemple (1, "Hadoop") puis (2, 1), etc...
    for colValue in list_colValues:
        # On ajoute le 2ème élément du tuple (c'est-à-dire la valeur) à la liste des valeurs de la colonne
        list_values.append(colValue[1])
    # On retourne un tuple de la forme (k3,v3)
    # avec "k3"=numéro de la colonne et "v3"=liste des valeurs de cette colonne
    # Par exemple, Reduce(1, [(1, "Hadoop"), (2, 1), (3, 3), (4, 5)]) retourne (1, ["Hadoop", 1, 3, 5])
    return (colNumber, list_values)

############ OUTPUT WRITER ############
# En appliquant la fonction Reduce à toutes les colonnes, on obtient une liste de la forme list(k3,v3).
# Cette liste présente donc pour chaque colonne k3, l'ensemble des valeurs qui y sont présentes (v3).
# Avec notre exemple, on obtient la liste suivante :
# [(1, ["Hadoop", 1, 3, 5]), (2, ["Spark", 2, 4, 6])]
# La dernière étape, "output writer" n'a alors qu'à écrire dans un fichier csv tous les éléments de la liste
# par ligne, prenant comme numéro de ligne la valeur k3 (c'est-à-dire le numéro de la colonne d'origine).
# On obtient ainsi le résultat suivant, qui correspond à une rotation du fichier d'origine :
# "Hadoop", 1, 3, 5
# "Spark", 2, 4, 6


